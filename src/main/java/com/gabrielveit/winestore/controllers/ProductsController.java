package com.gabrielveit.winestore.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gabrielveit.winestore.dtos.ProductDto;
import com.gabrielveit.winestore.services.ProductsService;

@RestController
@RequestMapping("/products")
public class ProductsController {
	private ProductsService productsService;
	
	public ProductsController(ProductsService productsService) {
		this.productsService = productsService;
	}
	
	@GetMapping("/recommendation")
	public ProductDto getProductRecommendation(String cpf) {
		return productsService.getProductRecommendation(cpf);
	}
}
