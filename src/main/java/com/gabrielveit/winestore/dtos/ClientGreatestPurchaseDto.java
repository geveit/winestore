package com.gabrielveit.winestore.dtos;

public class ClientGreatestPurchaseDto {
	private int id;
	private String nome;
	private String cpf;
	private double valorCompra;
	private String codigoCompra;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public double getValorCompra() {
		return valorCompra;
	}
	
	public void setValorCompra(double valorCompra) {
		this.valorCompra = valorCompra;
	}
	
	public String getCodigoCompra() {
		return codigoCompra;
	}
	
	public void setCodigoCompra(String codigoCompra) {
		this.codigoCompra = codigoCompra;
	}
}
