package com.gabrielveit.winestore.repositories.impl;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.gabrielveit.winestore.models.Purchase;
import com.gabrielveit.winestore.repositories.PurchasesRepository;

@Component
public class PurchasesRepositoryImpl implements PurchasesRepository {
	private List<Purchase> purchasesList;
	
	public List<Purchase> findAll() {
		return getPurchasesList();
	}
	
	private List<Purchase> getPurchasesList() {
		if (purchasesList == null) {
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://www.mocky.io/v2/598b16861100004905515ec7";
			
			ResponseEntity<List<Purchase>> response = restTemplate.exchange(
						url,
						HttpMethod.GET,
						null,
						new ParameterizedTypeReference<List<Purchase>>(){}
					);
			
			purchasesList = response.getBody();
		}
		
		return purchasesList;
	}
}
