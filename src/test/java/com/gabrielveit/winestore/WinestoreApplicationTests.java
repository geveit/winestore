package com.gabrielveit.winestore;

import org.junit.Assert;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.gabrielveit.winestore.dtos.ProductDto;
import com.gabrielveit.winestore.models.Product;
import com.gabrielveit.winestore.repositories.ClientsRepository;
import com.gabrielveit.winestore.repositories.PurchasesRepository;
import com.gabrielveit.winestore.repositories.impl.ClientsRepositoryTest;
import com.gabrielveit.winestore.repositories.impl.PurchasesRepositoryTest;
import com.gabrielveit.winestore.services.ClientsService;
import com.gabrielveit.winestore.services.ProductsService;

@RunWith(SpringRunner.class)
public class WinestoreApplicationTests {
	private ClientsRepository clientsRepository = new ClientsRepositoryTest();
	private PurchasesRepository purchasesRepository = new PurchasesRepositoryTest();
	
	private ClientsService clientsService;
	private ProductsService productsService;
	
	public WinestoreApplicationTests() {
		clientsService = new ClientsService(clientsRepository, purchasesRepository);
		productsService = new ProductsService(purchasesRepository);
	}

	@Test
	public void clientThatSpentMostIsFirst() {
		Assert.assertEquals(1, clientsService.getAllOrderedByValueSpent().get(0).getId());
	}
	
	@Test
	public void getClientWithGreatestPurchase() {
		Assert.assertEquals(3, clientsService.getClientWithGreatestPurchase(2016).getId());
	}
	
	@Test
	public void clientWithMostPurchasesIsFirst() {
		Assert.assertEquals(1, clientsService.getAllOrderedByNumberOfPurchases().get(0).getId());
	}
	
	@Test
	public void getProductRecommendation() {
		ProductDto product = productsService.getProductRecommendation("000.000.000.01");
		
		String productName = product.getProduto() + " " + product.getVariedade();
		
		Assert.assertEquals("Casa Silva Reserva Chardonnay", productName);
	}

}
