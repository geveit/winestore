package com.gabrielveit.winestore.dtos;

public class ClientFidelityDto {
	private int id;
	private String cpf;
	private String nome;
	private int numeroCompras;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getNumeroCompras() {
		return numeroCompras;
	}
	
	public void setNumeroCompras(int numeroCompras) {
		this.numeroCompras = numeroCompras;
	}
}
