package com.gabrielveit.winestore.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Client {
	private int id;
	private String nome;
	private String cpf;
	
	Client() {}
	
	public Client(int id, String name, String cpf) {
		this.id = id;
		this.nome = name;
		this.cpf = cpf;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nome=" + nome + ", cpf=" + cpf + "]";
	}
}
