package com.gabrielveit.winestore.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gabrielveit.winestore.dtos.ClientFidelityDto;
import com.gabrielveit.winestore.dtos.ClientGreatestPurchaseDto;
import com.gabrielveit.winestore.dtos.ClientTotalSpentDto;
import com.gabrielveit.winestore.services.ClientsService;

@RestController
@RequestMapping("/clients")
public class ClientsController {
	private final ClientsService clientsService;
	
	public ClientsController(ClientsService clientsService) {
		this.clientsService = clientsService;
	}
	
	@GetMapping("/ordered_by_total_spent")
	public List<ClientTotalSpentDto> getAllOrderedByTotalSpent() {
		return clientsService.getAllOrderedByValueSpent();
	}
	
	@GetMapping("/greatest_purchase")
	public ClientGreatestPurchaseDto getClientWithGreatestPurchase(int year) {
		return clientsService.getClientWithGreatestPurchase(year);
	}
	
	@GetMapping("/fidelity")
	public List<ClientFidelityDto> getAllOrderByNumberOfPurchases() {
		return clientsService.getAllOrderedByNumberOfPurchases();
	}
}
