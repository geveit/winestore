package com.gabrielveit.winestore.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
	private String produto;
	private String variedade;
	private String pais;
	private String categoria;
	private String safra;
	private double preco;
	
	public Product() {}
	
	public Product(String produto, String variedade, String pais, String categoria, String safra,
			double preco) {
		this.produto = produto;
		this.variedade = variedade;
		this.pais = pais;
		this.categoria = categoria;
		this.safra = safra;
		this.preco = preco;
	}

	public String getProduto() {
		return produto;
	}
	
	public void setProduto(String produto) {
		this.produto = produto;
	}
	
	public String getVariedade() {
		return variedade;
	}
	
	public void setVariedade(String variedade) {
		this.variedade = variedade;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getCategoria() {
		return categoria;
	}
	
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public String getSafra() {
		return safra;
	}
	
	public void setSafra(String safra) {
		this.safra = safra;
	}
	
	public double getPreco() {
		return preco;
	}
	
	public void setPreco(double preco) {
		this.preco = preco;
	}

	@Override
	public String toString() {
		return "Product [produto=" + produto + ", variedade=" + variedade + ", pais=" + pais
				+ ", categoria=" + categoria + ", safra=" + safra + ", preco=" + preco + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		Product product = (Product) obj;
		
		return (this.produto + " " + this.variedade).equals(product.produto + " " + product.variedade);
	}
}
