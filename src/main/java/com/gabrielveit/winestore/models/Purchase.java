package com.gabrielveit.winestore.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Purchase {
	private String codigo;
	private Date data;
	private String cliente;
	private List<Product> itens;
	private double valorTotal;
	
	public Purchase() {}
	
	public Purchase(String codigo, String data, String cliente, List<Product> itens, double valorTotal) {
		this.codigo = codigo;
		setData(data);
		this.cliente = cliente;
		this.itens = itens;
		this.valorTotal = valorTotal;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getData() {
		return data;
	}

	public void setData(String data) {
		try {
			this.data = new SimpleDateFormat("dd-MM-yyyy").parse(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public List<Product> getItens() {
		return itens;
	}

	public void setItens(List<Product> itens) {
		this.itens = itens;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	@Override
	public String toString() {
		return "Purchase [codigo=" + codigo + ", data=" + data + ", cliente=" + cliente + ", itens=" + itens
				+ ", valorTotal=" + valorTotal + "]";
	}
	
}
