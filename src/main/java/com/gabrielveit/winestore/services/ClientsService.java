package com.gabrielveit.winestore.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gabrielveit.winestore.dtos.ClientFidelityDto;
import com.gabrielveit.winestore.dtos.ClientGreatestPurchaseDto;
import com.gabrielveit.winestore.dtos.ClientTotalSpentDto;
import com.gabrielveit.winestore.models.Client;
import com.gabrielveit.winestore.models.Purchase;
import com.gabrielveit.winestore.repositories.ClientsRepository;
import com.gabrielveit.winestore.repositories.PurchasesRepository;

@Service
public class ClientsService {
	private ClientsRepository clientsRepository;
	private PurchasesRepository purchasesRepository;
	
	@Autowired
	public ClientsService(ClientsRepository clientsRepository, PurchasesRepository purchasesRepository) {
		this.clientsRepository = clientsRepository;
		this.purchasesRepository = purchasesRepository;
	}
	
	public List<ClientTotalSpentDto> getAllOrderedByValueSpent() {
		List<Client> clientsList = clientsRepository.findAll();
		List<Purchase> purchasesList = purchasesRepository.findAll();
		Map<String, Client> clientsMap = new HashMap<String, Client>();
		Map<String, Double> totalSpentMap = new HashMap<String, Double>();
		List<ClientTotalSpentDto> orderedClientsList = new ArrayList<ClientTotalSpentDto>();
		
		//Populate clients maps from clients list
		for (Client client : clientsList) {
			//Replace character to match purchases format
			String cpf = formatClientCpf(client);
			
			clientsMap.put(cpf, client);
			totalSpentMap.put(cpf, 0.0);
		}
		
		//Add purchases values to totalSpentMap
		for (Purchase purchase : purchasesList) {			
			//Most records have an extra zero
			String cpf = formatPurchaseCpf(purchase);
			
			double currentValue = totalSpentMap.get(cpf);
			
			totalSpentMap.put(cpf, currentValue + purchase.getValorTotal());
		}
		
		List<Map.Entry<String, Double>> totalSpentEntries = new ArrayList<Map.Entry<String, Double>>(totalSpentMap.entrySet());
		
		Collections.sort(
			totalSpentEntries,
			new Comparator<Map.Entry<String, Double>> () {

				@Override
				public int compare(Entry<String, Double> entry1, Entry<String, Double> entry2) {
					return Double.compare(entry2.getValue(), entry1.getValue());
				}
				
			}
		);
		
		//Populate return list
		for (Map.Entry<String, Double> entry : totalSpentEntries) {
			Client client = clientsMap.get(entry.getKey());
			
			ClientTotalSpentDto dto = new ClientTotalSpentDto();
			dto.setId(client.getId());
			dto.setNome(client.getNome());
			dto.setCpf(client.getCpf());
			dto.setTotal(entry.getValue());
			
			orderedClientsList.add(dto);
		}
		
		return orderedClientsList;
	}
	
	public ClientGreatestPurchaseDto getClientWithGreatestPurchase(int year) {
		List<Client> clientsList = clientsRepository.findAll();
		List<Purchase> purchasesList = purchasesRepository.findAll();
		
		Purchase greatestPurchase = null;
		Client greatestPurchaseClient = null;
		
		//Find the greatest purchase in 2016
		for (Purchase purchase : purchasesList) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(purchase.getData());
			
			int purchaseYear = calendar.get(Calendar.YEAR);
			double purchaseValue = purchase.getValorTotal();
			
			if ((greatestPurchase == null && purchaseYear == year) || 
					(purchaseYear == year && greatestPurchase.getValorTotal() < purchaseValue)) {
				greatestPurchase = purchase;
			}
		}
		
		//Format cpf
		String greatestPurchaseCpf = formatPurchaseCpf(greatestPurchase);
		
		
		//Find the client that made the greatest purchase
		for (Client client : clientsList) {
			//Replace character to match purchases format
			String cpf = formatClientCpf(client);
			
			if (cpf.equals(greatestPurchaseCpf)) {
				greatestPurchaseClient = client;
				break;
			}
		}
		
		ClientGreatestPurchaseDto dto = new ClientGreatestPurchaseDto();
		dto.setId(greatestPurchaseClient.getId());
		dto.setNome(greatestPurchaseClient.getNome());
		dto.setCpf(greatestPurchaseClient.getCpf());
		dto.setCodigoCompra(greatestPurchase.getCodigo());
		dto.setValorCompra(greatestPurchase.getValorTotal());
		
		return dto;
	}
	
	public List<ClientFidelityDto> getAllOrderedByNumberOfPurchases() {
		List<Client> clientsList = clientsRepository.findAll();
		List<Purchase> purchasesList = purchasesRepository.findAll();
		Map<String, Integer> numberPurchasesMap = new HashMap<String, Integer>();
		Map<String, Client> clientsMap = new HashMap<String, Client>();
		List<ClientFidelityDto> clientsOrderedList = new ArrayList<ClientFidelityDto>();
		
		//Populate maps
		for (Client client : clientsList) {
			//Replace character to match purchases format
			String cpf = formatClientCpf(client);
			
			clientsMap.put(cpf, client);
			numberPurchasesMap.put(cpf, 0);
		}
		
		//Find number of purchases for each client
		for (Purchase purchase : purchasesList) {
			String cpf = formatPurchaseCpf(purchase);
			
			int currentValue = numberPurchasesMap.get(cpf);
			
			numberPurchasesMap.put(cpf, ++currentValue);
		}
		
		List<Map.Entry<String, Integer>> numberPurchasesEntries = new ArrayList<Map.Entry<String, Integer>>(numberPurchasesMap.entrySet());
		
		Collections.sort(
			numberPurchasesEntries,
			new Comparator<Map.Entry<String, Integer>> () {

				@Override
				public int compare(Entry<String, Integer> entry1, Entry<String, Integer> entry2) {
					return Double.compare(entry2.getValue(), entry1.getValue());
				}
				
			}
		);
		
		for (Map.Entry<String, Integer> entry : numberPurchasesEntries) {
			Client client = clientsMap.get(entry.getKey());
			
			ClientFidelityDto dto = new ClientFidelityDto();
			dto.setId(client.getId());
			dto.setNome(client.getNome());
			dto.setCpf(client.getCpf());
			dto.setNumeroCompras(entry.getValue());
			
			clientsOrderedList.add(dto);
		}
		
		return clientsOrderedList;
	}
	
	private String formatPurchaseCpf(Purchase purchase) {
		String cpf;
		
		if (purchase.getCliente().length() > 14) {
			cpf = purchase.getCliente().substring(1);
		}
		else {
			cpf = purchase.getCliente();
		}
		
		return cpf;
	}
	
	private String formatClientCpf(Client client) {
		return client.getCpf().replace('-', '.');
	}
}
