package com.gabrielveit.winestore.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gabrielveit.winestore.dtos.ProductDto;
import com.gabrielveit.winestore.models.Product;
import com.gabrielveit.winestore.models.Purchase;
import com.gabrielveit.winestore.repositories.PurchasesRepository;

@Service
public class ProductsService {
	private PurchasesRepository purchasesRepository;
	
	public ProductsService(PurchasesRepository purchasesRepository) {
		this.purchasesRepository = purchasesRepository;
	}
	
	public ProductDto getProductRecommendation(String cpf) {
		List<Purchase> purchasesList = purchasesRepository.findAll();
		Purchase clientsLastPurchase = null;
		Product productToCompare = null;
		Product productRecommendation = null;
		
		//Find clients last purchase
		//Purchases are not ordered so we have to loop through all of them
		for (Purchase purchase : purchasesList) {
			if ((clientsLastPurchase == null & purchase.getCliente().contains(cpf)) ||
					(purchase.getCliente().contains(cpf) && purchase.getData().compareTo(clientsLastPurchase.getData()) > 0)) {
				clientsLastPurchase = purchase;
			}
		}
		
		//Get the first product in the list to use as base for recommendation
		productToCompare = clientsLastPurchase.getItens().get(0);
		
		//Find a purchase with a similar product
		for (Purchase purchase : purchasesList) {
			//Purchase from a different client and with different products
			if (!purchase.getCliente().equals(clientsLastPurchase.getCliente()) && purchase.getItens().size() > 1) {
				for (int i = 0; i < purchase.getItens().size(); i++) {
					Product product = purchase.getItens().get(i);
					
					if (product.equals(productToCompare)) {
						//Get a different product from the list
						if (i == 0) {
							productRecommendation = purchase.getItens().get(++i);
						}
						else {
							productRecommendation = purchase.getItens().get(0);
						}
						
						break;
					}
				}
			}
		}
		
		ProductDto dto = null;
		
		if (productRecommendation != null) {
			dto = new ProductDto();
			dto.setProduto(productRecommendation.getProduto());
			dto.setVariedade(productRecommendation.getVariedade());
			dto.setCategoria(productRecommendation.getCategoria());
			dto.setPais(productRecommendation.getPais());
			dto.setSafra(productRecommendation.getSafra());
			dto.setPreco(productRecommendation.getPreco());
		}
		
		return dto;
	}
}
