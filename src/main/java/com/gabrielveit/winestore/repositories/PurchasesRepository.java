package com.gabrielveit.winestore.repositories;

import java.util.List;

import com.gabrielveit.winestore.models.Purchase;

public interface PurchasesRepository {
	List<Purchase> findAll();
}
