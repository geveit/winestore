package com.gabrielveit.winestore.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import com.gabrielveit.winestore.models.Product;
import com.gabrielveit.winestore.models.Purchase;
import com.gabrielveit.winestore.repositories.PurchasesRepository;

public class PurchasesRepositoryTest implements PurchasesRepository {
	private List<Purchase> purchasesList;
	
	public PurchasesRepositoryTest() {
		purchasesList = new ArrayList<Purchase>();
		
		List<Product> productsList1 = new ArrayList<Product>();
		productsList1.add(new Product("Casa Silva Reserva", "Cabernet Sauvignon", null, null, null, 10));
		productsList1.add(new Product("Casa Silva Reserva", "Chardonnay", null, null, null, 10));
		purchasesList.add(new Purchase("12345", "19-02-2016", "000.000.000.01", productsList1, 20));
		
		List<Product> productsList2 = new ArrayList<Product>();
		productsList2.add(new Product("Casa Silva Reserva", "Cabernet Sauvignon", null, null, null, 10));
		productsList2.add(new Product("Casa Silva Reserva", "Chardonnay", null, null, null, 10));
		purchasesList.add(new Purchase("12345", "19-03-2016", "000.000.000.01", productsList2, 20));
		
		List<Product> productsList3 = new ArrayList<Product>();
		productsList3.add(new Product("Casa Silva Reserva", "Cabernet Sauvignon", null, null, null, 10));
		productsList3.add(new Product("Casa Silva Reserva", "Chardonnay", null, null, null, 10));
		purchasesList.add(new Purchase("12345", "19-04-2016", "000.000.000.02", productsList3, 20));
		
		List<Product> productsList4 = new ArrayList<Product>();
		productsList4.add(new Product("Casa Silva Reserva", "Cabernet Sauvignon", null, null, null, 10));
		productsList4.add(new Product("Casa Silva Reserva", "Chardonnay", null, null, null, 10));
		productsList4.add(new Product("Casa Silva Reserva", "Chardonnay", null, null, null, 10));
		purchasesList.add(new Purchase("12345", "19-05-2016", "000.000.000.03", productsList4, 30));
		
	}
	
	@Override
	public List<Purchase> findAll() {
		return purchasesList;
	}
	
}
