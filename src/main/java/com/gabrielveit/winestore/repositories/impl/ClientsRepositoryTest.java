package com.gabrielveit.winestore.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import com.gabrielveit.winestore.models.Client;
import com.gabrielveit.winestore.repositories.ClientsRepository;

public class ClientsRepositoryTest implements ClientsRepository {
	private List<Client> clientsList;
	
	public ClientsRepositoryTest() {
		clientsList = new ArrayList<Client>();
		clientsList.add(new Client(1, "Vinicius", "000.000.000-01"));
		clientsList.add(new Client(2, "Marcos", "000.000.000-02"));
		clientsList.add(new Client(3, "Joel", "000.000.000-03"));
	}

	@Override
	public List<Client> findAll() {
		return clientsList;
	}
}
