package com.gabrielveit.winestore.repositories.impl;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.gabrielveit.winestore.models.Client;
import com.gabrielveit.winestore.repositories.ClientsRepository;

@Component
public class ClientsRepositoryImpl implements ClientsRepository {
	private List<Client> clientsList;
	
	public List<Client> findAll() {
		List<Client> list = getClientsList();
		
		return list;
	}
	
	private List<Client> getClientsList() {
		if (clientsList == null) {
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://www.mocky.io/v2/598b16291100004705515ec5";
			
			ResponseEntity<List<Client>> response = restTemplate.exchange(
						url,
						HttpMethod.GET,
						null,
						new ParameterizedTypeReference<List<Client>>(){}
					);
			
			clientsList = response.getBody();
		}
		
		return clientsList;
	}
}
