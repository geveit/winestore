package com.gabrielveit.winestore.repositories;

import java.util.List;

import com.gabrielveit.winestore.models.Client;

public interface ClientsRepository {
	List<Client> findAll();
}
